package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void clear();

    void remove(Task task);

}
