package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project project);

    void clear();

    void remove(Project project);

}
