package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void clear();

    void remove(Task task);

}
