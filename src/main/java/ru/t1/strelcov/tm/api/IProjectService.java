package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void clear();

    void remove(Project project);

}
