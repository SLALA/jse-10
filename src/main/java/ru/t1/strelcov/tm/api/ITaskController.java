package ru.t1.strelcov.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

}
