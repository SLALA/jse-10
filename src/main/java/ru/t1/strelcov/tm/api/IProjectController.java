package ru.t1.strelcov.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
