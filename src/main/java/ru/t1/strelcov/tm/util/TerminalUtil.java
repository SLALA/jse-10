package ru.t1.strelcov.tm.util;

import java.util.Scanner;

public class TerminalUtil {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static String nextLine() {
        return SCANNER.nextLine();
    }

}
