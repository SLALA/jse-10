package ru.t1.strelcov.tm.controller;

import ru.t1.strelcov.tm.api.ITaskController;
import ru.t1.strelcov.tm.api.ITaskService;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList(){
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create(){
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear(){
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
