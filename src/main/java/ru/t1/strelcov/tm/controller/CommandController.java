package ru.t1.strelcov.tm.controller;

import ru.t1.strelcov.tm.api.ICommandController;
import ru.t1.strelcov.tm.api.ICommandService;
import ru.t1.strelcov.tm.model.Command;
import ru.t1.strelcov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String name = command.getArg();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Sla La");
        System.out.println("slala@slala.ru");
    }

    @Override
    public void displaySystemInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void exit() {
        System.exit(0);
    }



}
