package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.ITaskRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

}
