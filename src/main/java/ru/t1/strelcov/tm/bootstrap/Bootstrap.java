package ru.t1.strelcov.tm.bootstrap;

import ru.t1.strelcov.tm.api.*;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.controller.CommandController;
import ru.t1.strelcov.tm.controller.ProjectController;
import ru.t1.strelcov.tm.controller.TaskController;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;
import ru.t1.strelcov.tm.service.CommandService;
import ru.t1.strelcov.tm.service.ProjectService;
import ru.t1.strelcov.tm.service.TaskService;
import ru.t1.strelcov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(String... args) {
        displayWelcome();
        if (parseArgs(args))
            commandController.exit();
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseCommand(TerminalUtil.nextLine());
            System.out.println();
        }
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.displaySystemInfo();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

}
