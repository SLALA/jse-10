package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IProjectRepository;
import ru.t1.strelcov.tm.api.IProjectService;
import ru.t1.strelcov.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        final Project project;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty())
            project = new Project(name);
        else
            project = new Project(name, description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

}
