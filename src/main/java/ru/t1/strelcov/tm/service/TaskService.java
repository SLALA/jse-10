package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.ITaskRepository;
import ru.t1.strelcov.tm.api.ITaskService;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        final Task task;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty())
            task = new Task(name);
        else
            task = new Task(name, description);
        add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }
}
