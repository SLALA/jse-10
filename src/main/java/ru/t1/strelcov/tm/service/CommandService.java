package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.ICommandRepository;
import ru.t1.strelcov.tm.api.ICommandService;
import ru.t1.strelcov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
